const MinifyPlugin = require("babel-minify-webpack-plugin");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const Jarvis = require("webpack-jarvis");

module.exports = merge(common, {
  mode: "production",
  plugins: [
    new MinifyPlugin(),
    new Jarvis({
      port: 1337 // optional: set a port
    })
  ]
});
