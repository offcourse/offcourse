import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const rootEl = document.getElementById("app");

let render = () => {
  ReactDOM.render(<App />, rootEl);
};

if (module.hot) {
  const renderApp = render;
  const renderError = error => {
    const RedBox = require("redbox-react").default;
    ReactDOM.render(<RedBox error={error} />, rootEl);
  };

  render = () => {
    try {
      renderApp();
    } catch (error) {
      console.error(error);
      renderError(error);
    }
  };

  module.hot.accept("./App", () => {
    setTimeout(render);
  });
}

if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker
      .register("/service-worker.js")
      .then(registration => {
        console.log("SW registered: ", registration);
      })
      .catch(registrationError => {
        console.log("SW registration failed: ", registrationError);
      });
  });
}

render();
