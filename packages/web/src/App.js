import React, { Fragment, Component } from "react";
import {
  CourseCard,
  BasicButton,
  Menubar,
  Sidebar
} from "offcourse-ui-components";
import css from "./index.css";

const course = {
  courseId: "abc",
  goal: "Learn This",
  curator: "Offcourse",
  checkpoints: [
    {
      checkpointId: "a",
      task: "Do This",
      resourceUrl: "/"
    },
    {
      checkpointId: "b",
      task: "Do That",
      completed: true,
      resourceUrl: "/"
    },
    {
      checkpointId: "c",
      task: "Do More",
      resourceUrl: "/"
    }
  ],
  tags: ["tic", "tac", "toe"],
  description: `Gentrify adipisicing fanny pack pabst, health goth excepteur ut sunt swag qui plaid tumeric letterpress. Wolf gentrify live-edge 8-bit. Af ut thundercats locavore williamsburg, blue bottle man braid viral`
};

const onCheckpointToggle = ({ courseId, checkpointId, complete }) => {
  const checkpoint = course.checkpoints.find(
    ({ checkpointId: cid }) => checkpointId === cid
  );
  const status = complete ? "complete" : "incomplete";
  alert(`You marked "${checkpoint.task}" of "${course.goal}" as ${status}.`);
};

class App extends Component {
  constructor() {
    super();
    this.state = {
      visible: false
    };
    this.toggleVisible = this.toggleVisible.bind(this);
  }

  toggleVisible() {
    this.setState({ visible: !this.state.visible });
  }

  Buttons() {
    return [
      <BasicButton key="AAA" onClick={this.toggleVisible}>
        AAA
      </BasicButton>,
      <BasicButton key="BBB" onClick={this.toggleVisible}>
        BBB
      </BasicButton>,
      <BasicButton key="CCC" onClick={this.toggleVisible}>
        CCC
      </BasicButton>
    ];
  }
  render() {
    return (
      <Sidebar visible={this.state.visible} actions={this.Buttons()}>
        <Menubar onMenuClick={this.toggleVisible}>{this.Buttons()}</Menubar>
        <div
          style={{
            marginTop: "10px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <CourseCard course={course} onCheckpointToggle={onCheckpointToggle} />
        </div>
      </Sidebar>
    );
  }
}

export default App;
